/**
 * Created by Cosmin on 20.07.2017.
 */

var fs = require('fs');

/**
 * Log Requests
 *
 * @param req
 * @param res
 * @param next
 */
function logRequest(req, res, next) {

    var time = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');

    var log = time + ' ' + req.method + ' ' + req.url + '\r\n';

    fs.appendFile('./logs/log.txt', log, function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });

    next();
}

module.exports = logRequest;