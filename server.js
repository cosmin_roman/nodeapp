/**
 * Created by Cosmin on 20.07.2017.
 */

var app = require('./app');
var port = process.env.PORT || 3000;

app.listen(port, function () {
    console.log('App listening on port ' + port);
});