/**
 * Created by Cosmin on 20.07.2017.
 */

var mongoose = require('mongoose');
const db = "mongodb://localhost/api";

mongoose.connection.openUri(db);
mongoose.connection.once("open", function () {
    console.log("connection established");
}).on("error", function (error) {
    console.log(error);
});